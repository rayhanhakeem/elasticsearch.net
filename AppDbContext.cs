﻿using Elasticsearch.net.Models;
using Microsoft.EntityFrameworkCore;

namespace Elasticsearch.net
{
	public class AppDbContext : DbContext
	{
		public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
		{
		}

		public DbSet<Person> People { get; set; }
		// Tambahkan DbSet dan konfigurasi lain yang diperlukan untuk model Anda
	}
}
