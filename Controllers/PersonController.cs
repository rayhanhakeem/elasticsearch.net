﻿using Elasticsearch.Models;
using Microsoft.AspNetCore.Mvc;
using Nest;
using System.Linq;

namespace Elasticsearch.Controllers
{

	[Route("api/[controller]")]
	[ApiController]
	public class PersonController : ControllerBase
	{
		private readonly AppDbContext _dbContext;
		private readonly ElasticClient _elasticClient;

		public PersonController(AppDbContext dbContext, ElasticClient elasticClient)
		{
			_dbContext = dbContext;
			_elasticClient = elasticClient;
		}

		[HttpGet]
		public IActionResult GetPeople()
		{
			var peopleFromDatabase = _dbContext.People.ToList();
			var peopleFromElasticsearch = _elasticClient.Search<Person>(s => s
				.Index("people")
				.MatchAll()
			).Documents;

			// Lakukan logika lain yang diperlukan

			return Ok(peopleFromElasticsearch);
		}
	}
}
