using Elasticsearch.net.Models;

namespace Elasticsearch.net
{
	public class Program
	{
		public static void Main(string[] args)
		{
			var builder = WebApplication.CreateBuilder(args);

			// Add services to the container.

			builder.Services.AddControllers();
			// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
			builder.Services.AddEndpointsApiExplorer();
			builder.Services.AddSwaggerGen();

			var app = builder.Build();

			using (var scope = app.ApplicationServices.CreateScope())
			{
				var serviceProvider = scope.ServiceProvider;
				var elasticsearchClient = serviceProvider.GetRequiredService<ElasticClient>();

				if (!elasticsearchClient.Indices.Exists("string").Exists)
				{
					elasticsearchClient.Indices.Create("string", index => index
						.Map<Person>(x => x.AutoMap())
					);
				}
			}

			// Configure the HTTP request pipeline.
			if (app.Environment.IsDevelopment())
			{
				app.UseSwagger();
				app.UseSwaggerUI();
			}

			app.UseHttpsRedirection();

			app.UseAuthorization();


			app.MapControllers();

			app.Run();
		}
	}
}